from datetime import datetime
from django.contrib import admin
from django.core.cache import cache
from django.shortcuts import redirect
from django.urls import reverse

from .models import AttendanceRecord, Attendance
from .utils import get_payload

# Register your models here.


class AttendanceRecordAdmin(admin.ModelAdmin):
    date_hierarchy = 'due_time'
    exclude = ('extra_data',)
    search_fields = ('channel_id__exact', 'teacher_id__exact')
    list_display = ('id', 'channel', 'teacher', 'due_time')

    @admin.display(description=u'講義名')
    def channel(self, obj):
        # いちいち Slack API から情報を取得すると重くなったり頻度制限に引っかかったりする
        # キャッシュを使う、以下同じ
        cache_key = f"slack:channel:{obj.channel_id}:name"
        channel_name = cache.get(cache_key)
        if not channel_name:
            channel_name = get_payload(
                f"https://slack.com/api/conversations.info?channel={obj.channel_id}", {})['channel']['name']
            cache.set(cache_key, channel_name)
        return channel_name

    @admin.display(description=u'教員名')
    def teacher(self, obj):
        cache_key = f"slack:user:{obj.teacher_id}:name"
        teacher_name = cache.get(cache_key)
        if not teacher_name:
            teacher_name = get_payload(
                f"https://slack.com/api/users.info?user={obj.teacher_id}", {})['user']['real_name']
            cache.set(cache_key, teacher_name)
        return teacher_name

    def change_view(self, request, object_id, form_url='', extra_context=None):
        return redirect(reverse("admin:team_attendance_changelist") + f"?record_id={object_id}")

    def has_add_permission(self, _, obj=None): return False

    def has_change_permission(self, _, obj=None): return False

    def has_delete_permission(self, _, obj=None): return False


class AttendanceAdmin(admin.ModelAdmin):
    exclude = ('id', 'extra_data',)
    list_display = ('time_', 'student')
    list_display_links = None
    search_fields = ('record_id__exact',)

    @admin.display(description=u'受付時間')
    def time_(self, obj):
        return datetime.fromtimestamp(obj.time / 1000000)

    @admin.display(description=u'学生')
    def student(self, obj):
        student_name = cache.get(f"slack:user:{obj.uid}:display_name")
        if not student_name:
            student_name = get_payload(
                f"https://slack.com/api/users.info?user={obj.uid}", {})['user']['profile']['display_name']
            cache.set(f"slack:user:{obj.uid}:display_name", student_name)
        return student_name

    def has_module_permission(self, _): return False

    def has_add_permission(self, _, obj=None): return False

    def has_change_permission(self, _, obj=None): return False

    def has_delete_permission(self, _, obj=None): return False


admin.site.register(AttendanceRecord, AttendanceRecordAdmin)
admin.site.register(Attendance, AttendanceAdmin)
