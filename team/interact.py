from django.conf import settings
from django.http import HttpResponse
from django.core.exceptions import SuspiciousOperation
from django.template.loader import render_to_string as blocks
from django.urls import reverse
from django.utils.timezone import now
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST


from datetime import *
from .models import *
from .utils import *

import json
import time
import sys
import threading


@csrf_exempt
@require_POST
def entrypoint(request):
    payload = json.loads(request.POST.get('payload'))
    if payload.get('token') != settings.SLACK_API_VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    if payload.get('type') == "block_actions":
        return getattr(sys.modules[__name__], payload['actions'][0]['action_id'])(payload)
    return getattr(sys.modules[__name__], payload.get('type'))(payload)


@csrf_exempt
@require_POST
@check_verification_token
def attendance_check(request):
    time_span = parse_timespan(request.POST.get('text', '30s'))
    due_time = now() + time_span
    user_id = request.POST.get('user_id')
    channel_id = request.POST.get('channel_id')
    lecture_name = request.POST.get('channel_name').split('_')[0]

    rec = AttendanceRecord(
        teacher_id=user_id,
        channel_id=channel_id,
        due_time=due_time
    )
    rec.save()

    detail_url = request.build_absolute_uri(
        reverse('admin:team_attendancerecord_change', kwargs=dict(object_id=rec.id)))
    timer = threading.Timer(
        time_span.seconds, time_exceeded, args=[rec.id, lecture_name, time_span, detail_url])
    timer.start()

    chk_msg = get_payload(
        "https://slack.com/api/chat.postMessage",
        {
            'channel': channel_id,
            "blocks":
            blocks("blocks/attendance_check.txt",
                   {
                       'lecture_name': lecture_name,
                       'due_time': due_time,
                       'record_id': rec.id
                   })
        }
    )
    rec.extra_data['chk_msg_ts'] = chk_msg['ts']
    rec.save()

    post_message("https://slack.com/api/chat.postEphemeral",
                 {
                     'channel': channel_id,
                     'user': user_id,
                     "blocks": blocks(
                         "blocks/cancel_message.txt",
                         dict(record_id=rec.id))
                 })
    return HttpResponse()


def time_exceeded(record_id, lecture_name, time_span, detail_url):
    if AttendanceRecord.objects.filter(pk=record_id).count() == 0:
        return

    rec = AttendanceRecord.objects.get(pk=record_id)
    post_message("https://slack.com/api/chat.delete",
                 {
                     'channel': rec.channel_id,
                     'ts': rec.extra_data['chk_msg_ts'],
                     'as_user': True
                 })

    earliest_name = earliest_icon_url = None
    earliest = Attendance.objects.filter(record_id=record_id)
    if earliest.count() > 0:
        attnd = earliest.first()
        info = get_user_info(attnd.uid)
        earliest_name = info.get('real_name')
        earliest_icon_url = info.get('profile').get('image_32')

    last_minute_name = last_minute_icon_url = None
    last_minute = Attendance.objects.filter(
        record_id=record_id,
        time__gte=int((rec.due_time - time_span // 10).timestamp() * 1000000))
    if last_minute.count() > 0:
        attnd = last_minute.last()
        info = get_user_info(attnd.uid)
        last_minute_name = info.get('real_name')
        last_minute_icon_url = info.get('profile').get('image_32')

    post_message(
        "https://slack.com/api/chat.postMessage",
        {
            'channel': rec.channel_id,
            "blocks":
            blocks("blocks/time_exceeded.txt",
                   {
                       'earliest_name': earliest_name,
                       'earliest_icon_url': earliest_icon_url,
                       'last_minute_name': last_minute_name,
                       'last_minute_icon_url': last_minute_icon_url
                   })
        })

    post_message(
        "https://slack.com/api/chat.postMessage",
        {
            'channel': rec.teacher_id,
            "blocks":
            blocks("blocks/attendance_status.txt",
                   {
                       'detail_url': detail_url,
                       'lecture_name': lecture_name,
                       'presence_ratio': f'{earliest.count()} / #'
                   })
        })
    pass


def attnd_cancel(payload):
    action = payload['actions'][0]
    record_id = int(action['value'])
    rec = AttendanceRecord.objects.get(pk=record_id)
    post_message("https://slack.com/api/chat.delete",
                 {
                     'channel': rec.channel_id,
                     'ts': rec.extra_data['chk_msg_ts'],
                     'as_user': True
                 })
    rec.delete()
    post_message(payload.get('response_url'), {"delete_original": True})
    return HttpResponse()


def attend(payload):
    action = payload['actions'][0]
    action_ts = int(action['action_ts'].replace('.', ''))
    record_id = int(action['value'])

    if AttendanceRecord.objects.filter(pk=record_id).count() == 0:
        post_message("https://slack.com/api/chat.postEphemeral",
                     {
                         'channel': payload['channel']['id'],
                         'user': payload['user']['id'],
                         "text": "出席確認がすでに中止されました。"
                     })
    else:
        attnd, _ = Attendance.objects.update_or_create(
            record_id=record_id,
            uid=payload['user']['id'],
            defaults=dict(
                id=time.time_ns() << 64 | action_ts,
                time=action_ts))
        attnd.save()

        if Attendance.objects.filter(record_id=attnd.record_id).count() == 1:
            post_message("https://slack.com/api/chat.update",
                         {
                             'channel': payload['channel']['id'],
                             'ts': payload['message']['ts'],
                             "blocks":
                             blocks("blocks/attendance_check.txt",
                                    {
                                        'lecture_name': payload['channel']['name'].split('_')[0],
                                        'due_time': attnd.record.due_time,
                                        'record_id': attnd.record_id,
                                        'earliest_name': payload['user']['name']
                                    })
                         })

        post_message("https://slack.com/api/chat.postEphemeral",
                     {
                         'channel': payload['channel']['id'],
                         'user': payload['user']['id'],
                         "text": "出席できました。このメッセージを削除しても構いません。"
                     })

    return HttpResponse()
