from django.conf import settings
from django.core.exceptions import SuspiciousOperation
from functools import wraps

import json
import urllib
import datetime
import re


def check_verification_token(func):
    @wraps(func)
    def inner(request, *args, **kwargs):
        if request.POST['token'] != settings.SLACK_API_VERIFICATION_TOKEN:
            raise SuspiciousOperation('Invalid request.')
        return func(request, *args, **kwargs)
    return inner


def _get_request_object(url, data):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {settings.SLACK_API_BOT_OAUTH_TOKEN}'
    }
    return urllib.request.Request(url, json.dumps(data).encode(), headers)


def get_payload(url, data):
    req = _get_request_object(url, data)
    with urllib.request.urlopen(req) as res:
        return json.loads(res.read().decode('utf-8'))


def post_message(url, data):
    req = _get_request_object(url, data)
    with urllib.request.urlopen(req) as res:
        body = res.read()


def get_user_info(user_id):
    ret = get_payload(f"https://slack.com/api/users.info?user={user_id}", {})
    return ret.get('user')


RE_UNITS = re.compile(r'''\b(?P<qty>-?
			(?:\d+(?:\\.\d+|)|\b)\s*
			(?P<units>secs|mins|hour|days|sec|min|day|hr|dy|s|m|h|d)
		)\b''', re.IGNORECASE + re.VERBOSE)

TIME_UNITS = {
    'seconds': ['sec', 'secs', 's'],
    'minutes': ['min', 'mins', 'm'],
    'hours': ['hour', 'hours', 'hr', 'h'],
    'days': ['day', 'days', 'dy', 'd']
}


def parse_timespan(text):
    delta = datetime.timedelta()
    text = text.strip()
    m = RE_UNITS.search(text)
    if m is not None:
        units = m.group('units')
        quantity = text[:m.start('units')].strip()

        realunit = units
        for key, values in TIME_UNITS.items():
            if units in values:
                realunit = key
                break

        if realunit in ('days', 'hours', 'minutes', 'seconds'):
            delta = datetime.timedelta(**{realunit: int(quantity)})
    return delta
