import social_core.pipeline.user

def create_user(strategy, details, backend, user=None, *args, **kwargs):
    result = social_core.pipeline.user.create_user(strategy, details, backend, user=user, *args, **kwargs)
    # 新しい、かつ唯一のユーザのとき、超級管理者権限を与える
    if result['is_new'] and result['user'].__class__.objects.count() == 1:
        user = result['user']
        user.is_superuser = True
        user.is_staff = True
        user.save()
    return result
