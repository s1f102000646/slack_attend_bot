from __future__ import unicode_literals

from social_core.backends.slack import SlackOAuth2 as BaseSlackOAuth2

class SlackOAuth2(BaseSlackOAuth2):
    EXTRA_DATA = [
        ('user'),
        ('team')
    ]
    def user_data(self, access_token, *args, **kwargs):
        """Loads user data from service"""
        response = self.get_json('https://slack.com/api/users.identity',
                                # Slack API の最新仕様、token ではなく Authorization header
                                headers={'Authorization': 'Bearer %s' % access_token})
        if not response.get('id', None):
            response['id'] = response['user']['id']
        return response
