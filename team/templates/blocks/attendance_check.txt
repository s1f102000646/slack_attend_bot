[
	{
		"type": "header",
		"text": {
			"type": "plain_text",
			"text": "出席確認 Attendance Check"
		}
	},
	{
		"type": "section",
		"fields": [
			{
				"type": "mrkdwn",
				"text": ":books: *講義名 (Lecture):*\n{{ lecture_name }}"
			},
			{
				"type": "mrkdwn",
				"text": ":clock3: *締切時間 (Due Time):*\n{{ due_time|date:'m-d H:i:s' }}"
			}
		]
	},
	{
		"type": "section",
		"text": {
			"type": "mrkdwn",
			"text": ":speech_balloon: *今日の一言 (Phrase of the Day):*\nさて、今回は寝過ごして出席できなかった人、何人いるかな？"
		}
	},
	{
		"type": "divider"
	},
	{% if earliest_name %}
	{
		"type": "context",
		"elements": [
			{
				"type": "mrkdwn",
				"text": ":crown: *最速出席者　 The Earliest*"
			},
			{% if earliest_icon_url %}
			{
				"type": "image",
				"image_url": "{{ earliest_icon_url }}",
				"alt_text": "{{ earliest_name }}"
			},
			{% endif %}
			{
				"type": "plain_text",
				"text": "{{ earliest_name }}"
			}
		]
	},
	{% endif %}
	{% if last_minute_name %}
	{
		"type": "context",
		"elements": [
			{
				"type": "mrkdwn",
				"text": ":sweat: *駆け込み上手 Last-minute*"
			},
            {% if last_minute_icon_url %}
			{
				"type": "image",
				"image_url": "{{ last_minute_icon_url }}",
				"alt_text": "{{ last_minute_name }}"
			},
            {% endif %}
			{
				"type": "plain_text",
				"text": "{{ last_minute_name }}"
			}
		]
	},
	{% endif %}
	{
		"type": "actions",
		"elements": [
			{
				"type": "button",
				"text": {
					"type": "plain_text",
					"emoji": true,
					"text": ":raising_hand: 出席 I'm Here"
				},
				"style": "primary",
				"action_id": "attend",
				"value": "{{ record_id }}"
			}
		]
	}
]