[
		{
			"type": "header",
			"text": {
				"type": "plain_text",
				"text": "出席状況"
			}
		},
		{
			"type": "section",
			"fields": [
				{
					"type": "mrkdwn",
					"text": "*講義名:*\n{{ lecture_name }}"
				},
				{
					"type": "mrkdwn",
					"text": "*出席者数/欠席者数:*\n{{ presence_ratio }}"
				}
			]
		},
		{% if detail_url %} 
		{
			"type": "context",
			"elements": [
				{
					"type": "mrkdwn",
					"text": "*<{{ detail_url }}|詳細>*"
				}
			]
		}
		{% endif %}
]