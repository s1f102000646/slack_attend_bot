from django.db import models
import time

class Reply(models.Model):
    POSITIVE = 1
    NEUTRAL = 2
    NEGATIVE = 3

    user_name = models.CharField(max_length=100)
    user_id = models.CharField(max_length=100)
    response = models.IntegerField(default=NEUTRAL)

class AttendanceRecord(models.Model):
    class Meta:
        verbose_name = u'出席記録'
        verbose_name_plural = u'出席記録'

    # 出席記録 ID
    id = models.BigIntegerField(primary_key=True, default=time.time_ns, editable=False)
    # 教員の Slack Member ID
    teacher_id = models.CharField(max_length=20, editable=False, db_index=True)
    # 出席を取ったチャンネル ID
    channel_id = models.CharField(max_length=20, editable=False, db_index=True)
    # 締切時間
    due_time = models.DateTimeField(editable=False, db_index=True, verbose_name=u'締切時間')
    # 機能拡張用
    # 現時点では Bot が送ったメッセージの msg_ts を格納する
    extra_data = models.JSONField(default=dict, editable=False)

class Attendance(models.Model):
    # 出席 ID (time_ns と action_ts の合成)
    id = models.UUIDField(primary_key=True)
    # 出席の受付時間 (Slack から送られてきた action_ts)
    time = models.BigIntegerField()
    # 所属する出席記録のID
    record = models.ForeignKey(AttendanceRecord, db_index=True, on_delete=models.CASCADE)
    # 学生の Slack Member ID
    uid = models.CharField(max_length=20, db_index=True)
    # 機能拡張用
    extra_data = models.JSONField(default=dict)
