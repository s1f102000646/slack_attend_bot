from django.contrib import admin
from django.urls import path
from django.http import JsonResponse

from django.template.response import TemplateResponse
import django.utils.timezone
import time

class TeamAdminSite(admin.AdminSite):
    login_template = "admin/team/login.html"
    site_url = None
    site_header = u"管理者画面"

