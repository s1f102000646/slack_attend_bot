from django.contrib.admin.apps import AdminConfig


class TeamAdminConfig(AdminConfig):
    default_site = "team_app_sample.admin.TeamAdminSite"