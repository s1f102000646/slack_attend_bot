"""team_app_sample URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.views.generic.base import RedirectView
from team import views as team_views
from django.contrib.auth import views as auth_views

from team import interact

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('social_django.urls', namespace='social')),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('', RedirectView.as_view(pattern_name='admin:index'), name='index'),
    path('announce', team_views.announce, name='announce'),
    path('clear', team_views.clear, name='clear'),
    path('api/echo', team_views.echo, name='api_echo'),
    path('api/hello', team_views.hello, name='api_hello'),
    path('api/reply', team_views.reply, name='api_reply'),
    path('api/interact', interact.entrypoint, name='api_interact'),
    path('api/attnd_chk', interact.attendance_check, name='api_attnd_chk'),
]
